import React, { Component } from "react";
import { Conversation, ConversationResource } from '@atlaskit/conversation';


const provider = new ConversationResource({
    url: 'https://conversation-service/',
    user: [
        {
            id : "1234",
            name : "aravind"
        },
        {
            id:"4321",
            name : "Sam"
        }
    ]
  });


class NewConversation extends Component{
    constructor(props, context){
        super(props, context);
        this.state={}
    }

    componentDidMount(){}

    componentWillUnmount(){}

    render(){
        return(
            <div>
                <Conversation containerId="ari:cloud:platform::conversation/demo" provider={provider} />
            </div>
        );
    }
}

export default NewConversation